FROM node:slim as dev

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
RUN yarn install --silent
COPY . ./
CMD ["yarn", "build"]


FROM httpd:latest

WORKDIR /usr/local/apache2/htdocs/
COPY --from=dev /app/build .
CMD ["apachectl", "-D", "FOREGROUND"]

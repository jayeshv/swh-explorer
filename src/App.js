import React from 'react';

import Header from './Header';
import GraphiQLExplorer from './Explorer';

import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <GraphiQLExplorer />
    </div>
  );
}

export default App;

import { useExplorerPlugin } from '@graphiql/plugin-explorer';
import { createGraphiQLFetcher } from '@graphiql/toolkit';
import { GraphiQL } from 'graphiql';
import { useState } from 'react';

import 'graphiql/graphiql.css';
import '@graphiql/plugin-explorer/dist/style.css';

const DEFAULT_QUERY = `query getOrigins {
  origins(first: 2) {
    nodes {
      url
    }
  }
}
`;
const API_URL = 'http://127.0.0.1:8000/';

const fetcher = createGraphiQLFetcher({
  url: API_URL
});

function GraphiQLWithExplorer() {
  const [query, setQuery] = useState(DEFAULT_QUERY);
  const explorerPlugin = useExplorerPlugin({
    query,
    onEdit: setQuery,
  });
  return (
    <div className="explorer-container">
      <div className="graphiql-container">
        <GraphiQL
          fetcher={fetcher}
          query={query}
          onEditQuery={setQuery}
          /* plugins={[explorerPlugin]} */
        />
      </div>
    </div>
  );
}

export default GraphiQLWithExplorer;
